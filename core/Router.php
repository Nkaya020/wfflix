<?php

class Router
{
    public function direct($uri, $methodType)
    {
        // Haal de GET-waardes uit de url voor de router.
        $request = explode("?", $uri)[0];
        $split = explode("/", $request);
        $split[sizeof($split)] = '';
        switch ($split[1]) {
            case '':
            case 'home':
                HomeController::index();
                break;

            case 'account':
                switch ($split[2]) {
                    case 'update':
                        require('database/require.php');
                        if (!empty($_POST)) {
                            $db = db();
                            $user = User::fill($_POST);
                            if ($user->password === "") unset($user->password);
                            else $user->password = password_hash($user->password, PASSWORD_DEFAULT);
                            $_SESSION['username'] = $user->username;
                            $user->save($db);
                            header('location:/account/edit');
                            die();
                        }
                        break;
                    case 'new':
                        if (!empty($_POST)) {
                            AccountController::newUser();
                        } else {
                            AccountController::registration();
                        }
                        break;
                    case 'login':
                        if ($methodType == "POST") {
                            AccountController::postLogin();
                            break;
                        } else {
                            AccountController::login();
                        }
                        break;
                    case 'logout':
                        if (!isset($_SESSION['loggedIn'])) {
                            header('location:/account/login');
                            die();
                        }
                        AccountController::logout();
                        break;
                    case 'edit':
                        if (!isset($_SESSION['loggedIn'])) {
                            header('location:/account/login');
                            die();
                        }
                        AccountController::editMyAccount();
                        break;

                    case 'passwordreset':
                        require_once 'database/require.php';
                        $answerCorrect = false;
                        if (array_key_exists('username', $_POST) || array_key_exists('uid', $_POST)) {
                            $db = db();
                            $stmt = $db->prepare("SELECT * FROM users WHERE username = ? OR id = ?");
                            $stmt->execute([$_POST['username'], $_POST['uid']]);
                            $user = $stmt->fetch();
                            if (array_key_exists('uid', $_POST) && array_key_exists('answer', $_POST)) {
                                if (strtolower($user['questionAnswer'])
                                    === strtolower($_POST['answer'])) {
                                    $answerCorrect = true;
                                }
                                if (array_key_exists('pass1', $_POST) && array_key_exists('pass2', $_POST) && $answerCorrect) {
                                    if ($_POST['pass1'] !== $_POST['pass2']) {
                                        unset($_POST['pass1'], $_POST['pass2']);
                                        echo "wachtwoorden kloppen niet";
                                        include 'views/management/forgot-password.php';
                                    } else {
                                        $stmt = $db->prepare("UPDATE users SET password = ? WHERE users.id = ?");
                                        $stmt->execute([ password_hash($_POST['pass1'], PASSWORD_DEFAULT), $_POST['uid'] ]);
                                        header("location:/account/login");
                                        die();
                                    }
                                }
                            }
                        }
                        include 'views/management/forgot-password.php';
                        break;

                    case '':
                        AccountController::landingPage();
                        break;
                    default:
                        switch ($split[3]) {
                            case 'courses':
                                CourseController::myCourses($split[2]);
                                break;
                            case 'videos':
                                VideoController::myVideos($split[2]);
                                break;
                        }
                        break;
                }
                break;

            // end of account routing

            case 'admin':
                if (!isset($_SESSION["isAdmin"])) {
                    header("location:/error/403");
                    die();
                }
                switch ($split[2]) {
                    case 'accounts':
                        switch ($split[3]) {
                            case '':
                                AccountController::accountOverview();
                                break;
                            case 'update':
                                AccountController::accountUpdate();
                                break;
                        }
                        break;
                    case 'setup':
                        AdminController::setup();
                        break;
                }
                break;

            // end of admin routing

            case 'contact':
                ContactController::index();
                break;

            case 'registration':
                RegistrationController::index();
                break;

            case 'videos':
                switch ($split[2]) {
                    case '':
                        VideoController::allVideos();
                        break;
                    case 'new':
                        if (isset($_SESSION['isMaker'])) {
                            if ($methodType == "POST") {
                                VideoController::addVideo();
                            } else {
                                VideoController::newVideo();
                            }
                        } else {
                            header('location: /error/403');
                        }
                        break;
                    default:
                        VideoController::showVideo($split[2]);
                        break;
                }
                break;

            //end of video routing

            case 'courses':
                switch ($split[2]) {
                    case '':
                        CourseController::allCourses();
                        break;
                    case 'new':
                        if (isset($_SESSION['isMaker'])) {
                            if ($methodType == "POST") {
                                CourseController::addCourse();
                            } else {
                                CourseController::newCourse();
                            }
                        } else {
                            header('location: /error/403');
                        }
                        break;
                    default:
                        CourseController::showCourse($split[2]);
                        break;
                }
                break;

            //end of courses routing

            case 'search':
                SearchController::search($_GET['query']);
                break;

            case 'review':
                switch ($split[2]){
                    case 'add':
                        ReviewController::addReviewToVideo();
                        break;
                }
                break;

            //end of review routing

            case 'error':
                switch ($split[2]) {
                    case '403':
                        ErrorController::e403();
                        break;
                }
                break;


            // if all else fails, show error.
            default:
                ErrorController::e404();
                break;
        }
    }
}



