<?php
include 'views/fragments/prefix.php';
include 'views/fragments/header.php';
?>
<body class="bg-dark text-light">
<div class="container">
    <h2 class="mt-2">WFFlix: reset je wachtwoord</h2>
    <form method="post" action="/account/passwordreset">
        <?php if ($answerCorrect) { ?>
            <h4>Kies je nieuwe wachtwoord.</h4>
            <div class="input-group">
                <label class="input-group-text input-group-prepend">Wachtwoord</label>
                <input type="password" class="form-control" name="pass1" placeholder="Wachtwoord">
                <input type="password" class="form-control" name="pass2" placeholder="Bevestig wachtwoord">
                <input type="submit" class="btn btn-primary input-group-append" value="Afronden">
            </div>
            <input type="hidden" name="uid" value="<?= $user['id'] ?>">
            <input type="hidden" name="answer" value="<?= $user['questionAnswer'] ?>">
        <?php } else if (!isset($user)) { ?>
            <div class="input-group">
                <label class="input-group-prepend input-group-text">Gebruikersnaam:</label>
                <input class="form-control" type="text" name="username">
                <input type="submit" class="btn btn-primary input-group-append" value="Verder">
            </div>
        <?php } else { ?>
            <h4><?= $user['question'] ?></h4>
            <div class="input-group">
                <label class="input-group-prepend input-group-text">Antwoord:</label>
                <input type="text" class="form-control" name="answer">
                <input type="submit" class="btn btn-primary input-group-append" value="Verder">
            </div>
            <input type="hidden" name="uid" value="<?= $user['id'] ?>">
        <?php } ?>
    </form>
</div>
</body>