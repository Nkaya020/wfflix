<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
error_reporting(E_WARNING);
global $users, $destination;
?>
<body class="bg-dark text-light">
<div class="container">
    <h1 class="row"><span
                class="col-sm-3"><?php if (sizeof($users) === 1) echo "Mijn account"; else echo "Gebruikers" ?></span>
    </h1>
    <div class="row ">
        <div class="col-sm-1">ID</div>
        <div class="col-sm-2">Gebruikersnaam</div>
        <div class="col-sm-2">Wachtwoord</div>
        <?php if ($_SESSION['isMod']) { ?>
            <div class="col-sm-1">Moderator</div>
            <div class="col-sm-1">Maker</div>
        <?php } ?>
        <?php if ($_SESSION['isAdmin']) { ?>
            <div class="col-sm-1">Admin</div>
        <?php } ?>
    </div>
    <?php global $users;
    foreach ($users as $u) { ?>
        <form action="<?= $destination ?>" method="post" class="row">
            <div class="col-sm-1"><?= $u->id ?></div>
            <input type="hidden" value="<?= $u->id ?>" name="id">
            <div class="col-sm-2"><input class="admin-input input-group" name="username" type="text"
                                         value="<?= $u->username ?>"></div>
            <div class="col-sm-2"><input class="admin-input input-group" name="password" type="password"
                                         placeholder="Geheim" value=""></div>
            <?php if ($_SESSION['isMod']) { ?>
                <div class="col-sm-1">
                    <input type="hidden" name="isMod" value="0" />
                    <input class="admin-input input-group form-check-input" name="isMod"
                                             type="checkbox"
                        <?php if ($u->isMod) echo 'checked="checked"' ?>></div>
                <div class="col-sm-1">
                    <input type="hidden" name="isMaker" value="0" />
                    <input class="admin-input input-group form-check-input" name="isMaker"
                                             type="checkbox"
                    <?php if ($u->isMaker) echo 'checked="checked"' ?>">
                </div>
            <?php } ?>
            <?php if ($_SESSION['isAdmin']) { ?>
                <div class="col-sm-1">
                    <input type="hidden" name="isAdmin" value="0" />
                    <input class="admin-input input-group form-check-input" name="isAdmin"
                                             type="checkbox"
                    <?php if ($u->isAdmin) echo 'checked="checked"' ?>">
                </div>
            <?php } ?>
            <?php if (!sizeof($users) > 1) { ?>
                <div class="col-sm-2"><input class="input-group btn btn-light" type="submit" value="Update"></div>
            <?php } else { ?>
                <div class="col-sm-2 input-group">
                    <div class="input-group-prepend"><input class="input-group btn btn-light" type="submit" name="action"
                                                            value="Update"></div>
                </div>
            <?php } ?>
        </form>
        <hr/>
    <?php }
    error_reporting(E_ALL); ?>
</div>
</body>