<?php
include 'views/fragments/prefix.php';
?>
<body class="bg-dark text-light">
<?php include 'views/fragments/header.php'; ?>
<div class="container">
    <br> <br>
    <h2 align="center">Vul je nieuwe wachtwoord in.</h2>
    <form action="/account/testpasswordreset" method="POST">
        <br>
        <div class="col-4 offset-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Wachtwoord</span>
                </div>
                <input type="text" name="password" class="form-control ">
            </div>
        </div>
        <div class="col-4 offset-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Bevestig wachtwoord</span>
                </div>
                <input type="text" name="check" class="form-control">
            </div>
        </div>
        <div class="col-2 offset-6 mb-1">
            <input type="submit" class="btn btn-light input-group" value="Bevestig">
        </div>
    </form>
    <input type="hidden" name="step" value="3_setPass">
</div>
</body>

