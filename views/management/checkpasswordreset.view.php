<?php
include 'views/fragments/prefix.php';
?>
<body class="bg-dark text-light">
<?php include 'views/fragments/header.php'; ?>
<div class="container">
    <br> <br>
    <h2 align="center"> Beantwoord de volgende vraag: </h2>
    <h3 align="center"> <?php print_r($qID); ?> </h3>

    <br>
    <br>
    <h2 align="center">Vul hier je antwoord in.</h2>
    <form action="/account/passwordreset" method="POST">
        <div class="col-4 offset-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">antwoord</span>
                </div>
                <input type="text" name="anwser" class="form-control">
            </div>
        </div>
        <div class="col-2 offset-6 mb-1">
            <input type="submit" class="btn btn-light input-group" value="Bevestig">
        </div>
        <input type="hidden" name="step" value="2_checkAwnser">
        <input type="hidden" name="key1" value="<?= $pUID; ?>">
        <input type="hidden" name="key2" value="<?= $pAwns; ?>">
    </form>
</div>
</body>

