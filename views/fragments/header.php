<div class="header">
    <header id="header" class="sticky-top bg-light">
        <div class="row flex-nowrap justify-content-between align-items-center m-0">
            <div class="col-4 text-center">
                <a href="/"><h1 class="text-dark">WFFLIX</h1></a>
            </div>
            <!-- Load icon library searchbar-->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

            <!-- The form searchbar -->

            <form class="form-inline input-group" method="GET" action="/search">
                <input type="text" class="form-control" placeholder="Zoek.."
                       name="query" value="<?php if (isset($_GET['query'])) echo $_GET['query'] ?>">
                <div class="input-group-append">
                    <input class="input-group-append btn btn-outline-primary form-control" type="submit" value="🔎">
                </div>
            </form>

            <div class="col-4 d-flex justify-content-end align-items-center">
                <?php if (!isset($_SESSION['loggedIn'])) {?>
                    <a id="loginButton" class="btn text-dark btn-sm btn-outline-dark" href="/account/login">Log In</a>
                <?php } else { ?>
                    <a class="btn text-dark btn-sm btn-outline-dark" href="/account"><?=$_SESSION['username']?></a>
                <?php } ?>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-top">
            <a class="navbar-brand mx-auto" href="#">Pages:</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="navbar-nav mx-auto">
                    <a class="p-2 text-muted" href="/">Home</a>
                    <a class="p-2 text-muted" href="/videos">Videos</a>
                    <a class="p-2 text-muted" href="/courses">Courses</a>
                </div>
            </div>
        </nav>
    </header>

</div>
<?php if (isset($_GET['fail'])) { ?>
    <div class="row-flex">
        <div class="col-md-12 bg-danger text-center p-15">
            Verkeerde inloggegevens. Probeer het nog een keer.
        </div>
    </div>
<?php } if(isset($_GET['success'])) {  ?>
    <div class="row-flex">
        <div class="col-md-12 bg-success text-center p-15">
            Gelukt!
        </div>
    </div>
<?php } ?>