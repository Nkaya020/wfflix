<br>
<div class="review">
    <?php if (array_key_exists('loggedIn', $_SESSION)) { ?>
    <!-- Place Review form -->
    <form action="/review/add" method="POST">
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Wat vond je van de video?</label>
            <textarea name="text" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Typ hier je recentie..."></textarea>
        </div>
        <input type="hidden" value="<?= $reviews[0]["v.id"] ?>" name="videoId">
        <input type="hidden" value="<?= $_SERVER['REQUEST_URI'] ?>" name="redirect">
        <button type="submit" class="btn btn-primary">Plaats recentie</button>
    </form>
    <!-- End Place Review form -->
    <?php } ?>
    <!-- User review -->
    <?php
    if($reviews[0]['.hasReviews']){
        foreach ($reviews as $review){
    ?>
    <div class="card">
        <div class="card-header" style="color: black">
            <?= $review['u.username'] ?>
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0" >
                <p style="color: black"><?= $review['r.text'] ?></p>
                <footer class="blockquote-footer">Geplaatst op: <?= $review['r.createdAt'] ?></footer>
            </blockquote>
        </div>
    </div>
    <br>
    <?php }} ?>
    <!-- End User review -->
</div>