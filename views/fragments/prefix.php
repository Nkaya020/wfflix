<?php
global $TITLE;
?>
<head>
    <title><?= $TITLE ?></title>
    <?php
    $devmode = parse_ini_file("config.ini", true)[gethostname()]['devmode'];
    $offset = strlen($_SERVER['DOCUMENT_ROOT']);

    // Load all css and js in the "scripts" and "styles" folders
    foreach (glob($_SERVER['DOCUMENT_ROOT'] . "/styles/*.css") as $css) {
        $location = substr($css, $offset)
        ?>
        <link rel="stylesheet" href="<?= $location ?>">
    <?php }
    foreach (glob($_SERVER['DOCUMENT_ROOT'] . "/scripts/*.js") as $script) {
        $location = substr($script, $offset)
        ?>
        <script src="<?= $location ?>"></script>
    <?php }


    // Load all css in either vendor/development or vendor/production, depending on the settings
    $path = $_SERVER['DOCUMENT_ROOT'] . ($devmode ? "/vendor/development/" : "/vendor/production/");
    foreach (glob($path . "*.css") as $css) {
        $location = substr($css, $offset)
        ?>
        <link rel="stylesheet" href="<?= $location ?>">
    <?php }
    foreach (glob($path . "*.js") as $script) {
    $location = substr($script, $offset)
    ?>
        <script src="<?= $location ?>"></script>
        <?php } ?>
</head>
