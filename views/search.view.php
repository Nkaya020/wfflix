<?php include 'views/fragments/prefix.php';
include 'views/fragments/header.php';
?>
<body class="bg-dark text-light">
<div class="container">
    <?php if (!empty($videos)) {?>
        <h2 class="mt-1">Video's die te maken hebben met: <?= $_GET['query'] ?></h2>
        <div class="card-deck mt-2">
            <?php foreach ($videos as $video) { ?>
                <a href="/videos/<?= $video['id'] ?>" class="card mb-3" style="min-width: 20rem;">
                    <div class="card-body">
                        <img class="card-img-top"
                             src=<?= $video['thumbnail'] ?>>
                    </div>
                    <div class="card-footer rounded-bottom bg-light text-dark border-top border-dark">
                        <h5 class="text-center mt-1"><?= $video['title'] ?></h5>
                    </div>
                </a>
            <?php } ?>
        </div>
        <?php } ?>
    <?php if (!empty($courses)) {?>
        <h2 class="mt-1">Courses die te maken hebben met: <?= $_GET['query'] ?></h2>
        <div class="card-deck mt-2">
            <?php foreach ($courses as $course) { ?>
                <a href="/courses/<?= $course['id'] ?>" class="card mb-3" style="min-width: 20rem;">
                    <div class="card-body">
                        <img class="card-img-top"
                             src=<?= $course['thumbnail'] ?>>
                    </div>
                    <div class="card-footer rounded-bottom bg-light text-dark border-top border-dark">
                        <h5 class="text-center mt-1"><?= $course['name'] ?></h5>
                    </div>
                </a>
                <?php } ?>
        </div>
    <?php }
    if (empty($courses) && empty($videos)) { ?>
        <h2 class="mt-1">Sorry, hier hebben we niks over!</h2>
    <?php } ?>
</div>
</body>