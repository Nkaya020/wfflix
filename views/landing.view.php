<?php include "views/fragments/prefix.php" ?>
<?php include "views/fragments/header.php" ?>
<body class="bg-dark text-light">
<div class="container">
    <h2>Welkom, <?= $_SESSION['username'] ?>!</h2>
    <div class="row">
        <a class="mx-3 col-md-2 btn btn-light" href="/account/logout">Log uit</a>
        <a class="mx-3 col-md-2 btn btn-light" href="/account/edit">Accountdetails</a>
    </div>
    <?php if ($_SESSION['isMod']) { ?>
        <h3 class="m-2">Moderatorfuncties:</h3>
        <div class="row">
<!--            <a class="mx-3 col-md-2 btn btn-light" href="/course/overview">Course Management</a>-->
        </div>
    <?php }
    if ($_SESSION['isMaker']) { ?>
        <h3 class="m-2">Makerfuncties:</h3>
        <div class="row">
            <a class="mx-3 col-md-2 btn btn-light" href="/courses/new">Nieuwe Course</a>
            <a class="mx-3 col-md-2 btn btn-light" href="/videos/new">Nieuwe Video</a>
            <a class="mx-3 col-md-2 btn btn-light" href="/account/<?= $_SESSION['uid'] ?>/videos">Mijn Videos</a>
            <a class="mx-3 col-md-2 btn btn-light" href="/account/<?= $_SESSION['uid'] ?>/courses">Mijn Courses</a>
        </div>
    <?php }
    if ($_SESSION['isAdmin']) { ?>
        <h3 class="m-2">Administratiefuncties:</h3>
        <div class="row">
            <a class="mx-3 col-md-2 btn btn-light" href="/admin/accounts">User Management</a>
            <a class="mx-3 col-md-2 btn btn-danger" href="/admin/setup">Reset WFFlix</a>
        </div>
    <?php } ?>
</div>
</body>

