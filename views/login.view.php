<?php include "views/fragments/prefix.php"; ?>
<body class="bg-dark text-light">
<?php include "views/fragments/header.php"; ?>
<div class="container">
    <form action="/account/login" method="POST" class="card-body">
        <h2 class="col-lg-6 offset-lg-3 mb-3">Welkom bij WFFlix!</h2>
        <div class="input-group input-group col-lg-6 offset-lg-3 mb-3">
            <div class="input-group-prepend">
                <label for="usernameInput" class="input-group-text">Gebruikersnaam</label>
            </div>
            <input class="form-control" id="usernameInput" placeholder="Jonathan" type="text" name="username">
        </div>
        <div class="input-group  col-lg-6 offset-lg-3 mb-3">
            <div class="input-group-prepend">
                <label for="passwordInput" class="input-group-text">Wachtwoord</label>
            </div>
            <input class="form-control" id="passwordInput" placeholder="Windesgeheim" type="password" name="password">
        </div>
        <div class="input-group col-lg-6 offset-lg-3">
            <a class="btn btn-info form-control mr-1" href="/account/new">Registreren</a>
<!--            <input class="form-control bg-dark border-0" disabled="disabled">-->
            <a class="btn btn-info form-control mr-1" href="/account/passwordreset">Vergeten?</a>
            <input class="btn btn-success form-control" type="submit" value="Log In">
        </div>
    </form>
</div>
<!--    <div class="row">-->
<!--        <form action="/account/login" method="POST" class="form-signin">-->
<!--            <h1 class="h3 mb-3 font-weight-normal">Inloggen</h1>-->
<!--            <label for="uname" class="sr-only">User name </label>-->
<!--            <input name="username" type="text" id="inputusername" class="form-control" placeholder="Enter username "-->
<!--                   required autofocus>-->
<!---->
<!--            <label for="inputPassword" class="sr-only">Wachtwoord</label>-->
<!--            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password"-->
<!--                   required>-->
<!--            <div class="checkbox mb-3">-->
<!--                <label>-->
<!--                    <input type="checkbox" value="remember-me"> Remember me-->
<!--                </label>-->
<!--            </div>-->
<!--            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>-->
<!--            <p class="mt-5 mb-3 text-muted">&copy; 2020</p>-->
<!--        </form>-->
<!--    </div>-->
</body>
