<?php
include 'views/fragments/prefix.php';
?>
<body class="bg-dark text-light">
<?php include 'views/fragments/header.php'; ?>
<div class="container">
    <h1>Wat leuk dat je met ons wil leren!</h1>
    <form action="/account/new" method="POST" class="inline-form border rounded p-4 row">
        <div class="col-md-12 col-lg-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Gebruikersnaam:</span>
                </div>
                <input type="text" name="username" class="form-control" name="username">
            </div>
        </div>
        <div class="col-md-12 col-lg-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Wachtwoord:</span>
                </div>
                <input type="password" name="password" class="form-control" name="username">
            </div>
        </div>
        <div class="col-md-12 col-lg-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Bevestig wachtwoord:</span>
                </div>
                <input type="password" name="password2" class="form-control" name="username">
            </div>
        </div>
        <div class="col-md-12 col-lg-8 mt-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Geef jouw persoonlijke beveiligingsvraag:</span>
                </div>
                <input type="text" name="question" class="form-control" name="username">
            </div>
        </div>
        <div class="col-md-12 col-lg-4 mt-4 mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Beandwoord jouw vraag:</span>
                </div>
                <input type="text" name="questionAnswer" class="form-control" name="username">
            </div>
        </div>
        <div class="col-md-6 col-lg-4 mt-4 offset-md-6 offset-lg-8">
            <input type="submit" class="btn btn-success input-group" value="Wordt lid!">
        </div>
    </form>
</div>
</body>

