<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
?>
<body class="bg-dark text-light">
<div class="container">
    <h1><?= $reviews[0]["v.title"] ?> <small class="text-muted">gemaakt door: <?= $reviews[0]['o.maker'] ?></small></h1>
    <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src=<?= $reviews[0]['v.url'] ?> allowfullscreen></iframe>
    </div>
    <br>
    <h7 class="text-white"  ><?= $reviews[0]["v.description"] ?></h7>
    <br>
<?php
include "views/fragments/review.php";
?>
</div>
</body>
