<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
?>
<body class="bg-dark text-light">
<div class="container">
    <?php if(isset($course)){ ?>
        <h1><?= $course->name ?> <small class="text-muted">gemaakt door: <?= $owner->username ?></small></h1>

        <body><?= $course->description ?></body>
    <?php } ?>
    <h1>Video's</h1>
    <div class="card-deck">
        <?php foreach ($videos as $video){ ?>
                <a href="/videos/<?= $video->id ?>" class="card mb-3" style="min-width: 20rem;">
                    <div class="card-body">
                        <img class="card-img-top"
                             src=<?= $video->thumbnail ?>>
                    </div>
                    <div class="card-footer rounded-bottom bg-light text-dark border-top border-dark">
                        <h5 class="text-center mt-1"><?= $video->title ?></h5>
                    </div>
                </a>
        <?php } ?>
    </div>
</div>
</body>