<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
?>
<body class="bg-dark text-light">
<div class="container">
    <?php if(count($courses) > 0){ ?>
    <form method="POST" action="/videos/new">
        <div class="form-group">
            <label for="exampleFormControlInput1">Video titel</label>
            <input name="title" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Mijn nieuwe video">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Video link</label>
            <input name="url" type="text" class="form-control" id="exampleFormControlInput1" placeholder="https://www.youtube.com/mijnvideo">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Thumbnail link</label>
            <input name="thumbnail" type="text" class="form-control" id="exampleFormControlInput1" placeholder="https://www.plaatje.com">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Beschrijving</label>
            <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Toevoegen aan cursus? </label>
            <select name="course" class="form-control" id="exampleFormControlSelect1">
                <?php foreach ($courses as $course){ ?>
                <option value=<?= $course->id ?>><?= $course->name ?></option>
                <?php } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php } else{ ?>
        <br>
        <h4>Je moet eerst een cursus aanmaken voordat je een video kan uploaden. Klik <a href="/courses/new">hier</a> om een cursus aan te maken.</h4>
    <?php } ?>
</div>
</body>