<?php include "views/fragments/prefix.php" ?>
<body class="bg-dark">
<?php include "views/fragments/header.php" ?>
<div class="container">
    <?php if(!isset($_SESSION['loggedIn'])) { ?>

    <div class="row">
        <div class="col-12">
            <div class="jumbotron p-3 p-md-5 rounded bg-transparent">
                <div class="col-12" style="background-color: rgba(0,0,0, 0.4);">
                    <h1 class="display-4 text-white">Wfflix place to be!</h1>
                    <p class="lead text-light my-3">Teachers say it starts at home! You should begin coding.</p>
                </div>
            </div>
        </div>
    </div>
   <?php } else {?>
    <div class="card-deck">
    </div>
        <div class="mt-5"></div>
   <?php }?>

    <h1 style="color: white">Meest recente video's</h1>
    <div class="card-deck">
        <?php foreach ($videos as $video){ ?>
            <a href="/videos/<?= $video->id ?>" class="card mb-3" style="min-width: 20rem;">
                <div class="card-body">
                    <img class="card-img-top"
                         src=<?= $video->thumbnail ?>>
                </div>
                <div class="card-footer rounded-bottom bg-light text-dark border-top border-dark">
                    <h5 class="text-center mt-1"><?= $video->title ?></h5>
                </div>
            </a>
        <?php } ?>
    </div>
    <br>

    <div class="row mb-2">
        <div class="col-12 col-md-6">
            <div class="card mb-4 box-shadow">
                <img class="" src="" alt="">
                <div class="card-body d-flex flex-column align-items-start">
                    <p class="d-inline-block mb-2 text-info font-weight-bold">About</p>
                    <h3 class="mb-0">
                        <a class="text-dark" >Wie zijn wij en waar staan wij voor?</a>
                    </h3>
                    <div class="mb-1 text-muted">30 september</div>
                    <p class="card-text">Geen casa de papel, maar programmeer courses om op je eigen tempo te volgen.</p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card mb-4 box-shadow">
                <img class="" src="" alt="">
                <div class="card-body d-flex flex-column align-items-start">
                    <p class="d-inline-block mb-2 text-warning font-weight-bold">Contact</p>
                    <h3 class="mb-0">
                        <a class="text-dark" >Contact gegevens</a>
                    </h3>
                    <div class="mb-1 text-muted">30 september</div>
                    <p class="card-text">Voor hulp kan je altijd contact op nemen met de servicedesk Windesheim.</p>
                </div>
            </div>
        </div>
    </div>

    <!--div class="row">
         <div class="col-12">
             <div class="p-3 mb-3 bg-light rounded">
                 <h4>About</h4>
                 <p class="mb-0"><span class="font-italic">Your news fast and accurate!</span> At News, we take the news very
                     seriously. We guarantee real news from certified, experience journalists.</p>
             </div>
         </div>
     </--div>-->

    <!--<div class="row">
        <div class="col-6">
            <h4 class="font-italic">Archives</h4>
            <ol class="list-unstyled">
                <li><a href="#">October 2018</a></li>
                <li><a href="#">September 2018</a></li>
                <li><a href="#">August 2018</a></li>
                <li><a href="#">July 2018</a></li>
            </ol>
        </div>
        <div class="col-6">
            <h4 class="font-italic">Social</h4>
            <ol class="list-unstyled">
                <li><a href="#">YouTube</a></li>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Facebook</a></li>
            </ol>
        </div>
    </div>
</div> -->
</body>

</html>
