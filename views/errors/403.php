<?php include "views/fragments/prefix.php" ?>
<body class="bg-dark text-light">
<?php include "views/fragments/header.php" ?>
<div class="container justify-content-center">
    <div class="row align-self-center h-15 mt-5">403. Hier mag je dus niet zijn.</div>

    <a href="/home" class="row col-md-3 btn-light btn align-self-center text-center mt-5">
            <h1><i class="fa-arrow-circle-o-left fa-lg"></i>Terug naar WFFlix</h1>
    </a>
</div>
