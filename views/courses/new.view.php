<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
?>
<body class="bg-dark text-light">
<div class="container">
    <form method="POST" action="/courses/new">
        <div class="form-group">
            <label for="exampleFormControlInput1">Naam Cursus</label>
            <input name="name" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Mijn nieuwe cursus">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Thumbnail link</label>
            <input name="thumbnail" type="text" class="form-control" id="exampleFormControlInput1" placeholder="https://www.plaatje.com">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Beschrijving</label>
            <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Categorie</label>
            <select class="form-control" id="exampleFormControlSelect1">
                <option>Geen</option>
                <option>Course 1</option>
                <option>CSS</option>
                <option>C#</option>
                <option>Java</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>