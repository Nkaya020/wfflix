<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
?>
<body class="bg-dark text-light">
<div class="container">
    <h1>Courses</h1>
    <div class="card-deck">
        <?php foreach ($courses as $course){ ?>
                <a href="/courses/<?= $course->id ?>" class="card mb-3" style="min-width: 20rem;">
                    <div class="card-body">
                        <img class="card-img-top"
                             src=<?= $course->thumbnail ?>>
                    </div>
                    <div class="card-footer rounded-bottom bg-light text-dark border-top border-dark">
                        <h5 class="text-center mt-1"><?= $course->name ?></h5>
                    </div>
                </a>
        <?php } ?>
    </div>
</div>
</body>