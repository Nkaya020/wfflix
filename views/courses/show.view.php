<?php include "views/fragments/prefix.php";
include "views/fragments/header.php";
?>
<body class="bg-dark text-light">
<div class="container">
    <h1><?= $video->title ?></h1>
    <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src=<?= $video->url ?> allowfullscreen></iframe>
    </div>
</div>
</body>