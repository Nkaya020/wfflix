<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact</title>
    <link href="/views/contact.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <form action="action_page.php">

        <label for="fname">First Name</label>
        <input type="text" id="fname" name="firstname" placeholder="Your name..">

        <label for="lname">Last Name</label>
        <input type="text" id="lname" name="lastname" placeholder="Your last name..">

        <label for="inputEmail" class="sr-only">Email Adres </label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email Adres " required autofocus>

        <label for="country">Country</label>
        <select id="country" name="country">
            <option value="Nederland">Nederland</option>
            <option value="Belgie">Belgie</option>
        </select>

        <label for="subject">Subject</label>
        <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

        <input type="submit" value="Submit">

    </form>
</div>
</body>
</html>