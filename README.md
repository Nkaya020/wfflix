# WFFlix

#Startdata
Na initialisatie zijn er twee accounts (user :: pass):

`admin` :: `tSHnH2SiapgmeS6xnyTiGeGAVa5Py53x`

`user` :: `aEDUHD9aSiKMHzj7MHu8X3twviTQxath`

Je kan dit aanpassen in `./database/data/00-account.sql`. Wachtwoorden worden gehasht na setup.

#Quickstart
0. Download of clone het project en zet het in een webfolder.
1. Maak een database. Je mag zelf een naam kiezen maar wij raden `wfflix` aan.
2. Voeg de relevante gegevens toe aan `./config.ini`
   - `[hostname]`: de hostname van de computer. Als je dit niet weet kan je deze krijgen met de PHP-functie `gethostname();`
   - `host`: het IP-adres of domein van de server. Dit kan ook localhost zijn.
   - `dbname`: de databasenaam die je in stap 1 hebt toegewezen.
   - `user`: de username van een MYSQL-gebruiker met volledige rechten over de database.
   - `password`: het wachtwoord van deze MYSQL-gebruiker.
   - `devmode`: zet deze naar 'true' om development javascript en css bij te voegen in plaats van de minified versies.
3. Voer `./setup.php` uit in console. Voorbeeld: `php -f ./setup.php`. Deze is niet uitvoerbaar vanuit een webserver.
4. ???
5. Profit

#Structuur
De folder `./database/` bevat alle informatie voor het verbinden, instellen en uitlezen van de database.
De subfolders `data`, `relations` en `structure` staan voor de data, foreign keys en tables respectievelijk.
`require.php` is het bestand dat je kan requiren in php (of includen, of -once) om een snel begin te maken
met databasedevelopment.
`controllers` bevat controllers, `docs` bevat gerelateerde documenten, `fragments` bevat HTML-segmenten die meerdere malen worden gebruikt in views.
Deze webpagina's staan overegens in `views`. `model` bevat helperclasses voor databaseinteractie met objecten.