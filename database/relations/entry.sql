ALTER TABLE `entries`
ADD FOREIGN KEY (userId) REFERENCES users(id),
ADD FOREIGN KEY (courseId) REFERENCES courses(id);
