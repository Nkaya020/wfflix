ALTER TABLE `reviews`
ADD FOREIGN KEY (userId) REFERENCES users(id),
ADD FOREIGN KEY (courseId) REFERENCES courses(id),
ADD FOREIGN KEY (videoId) REFERENCES videos(id);
