ALTER TABLE `courses`
ADD FOREIGN KEY (makerId) REFERENCES users(id),
ADD FOREIGN KEY (category) REFERENCES categories(id);
