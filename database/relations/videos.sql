ALTER TABLE `videos`
ADD FOREIGN KEY (makerId) REFERENCES users(id),
ADD FOREIGN KEY (course) REFERENCES courses(id);
