create table users
(
	id INT auto_increment,
	username VARCHAR(32) not null,
	password VARCHAR(255) not null,
	isMod boolean default 0 not null,
	isMaker boolean default 0 not null,
	isAdmin boolean default 0 not null,
    question VARCHAR(255) NOT NULL,
    questionAnswer VARCHAR(255) NOT NULL,
	primary key (id)
);


