CREATE TABLE categories (
       `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
       `name` VARCHAR(32) UNIQUE NOT NULL,
       `description` VARCHAR(128),
       PRIMARY KEY(id)
       );