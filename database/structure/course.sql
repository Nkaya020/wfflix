CREATE TABLE courses (
    `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
    `thumbnail` VARCHAR(256) NOT NULL,
    `name` VARCHAR(32) UNIQUE NOT NULL,
    `description` TEXT,
    `makerId` INT NOT NULL,
    `difficulty` INT NOT NULL DEFAULT 5,
    `category` INT,
    PRIMARY KEY (id)
);
