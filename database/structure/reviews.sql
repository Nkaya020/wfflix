CREATE TABLE reviews (
    `id` INT NOT NULL UNIQUE AUTO_INCREMENT,
    `userId` INT NOT NULL,
    `courseId` INT,
    `videoId` INT,
    `text` TEXT NOT NULL,
    `title` VARCHAR(64),
    `forVideo` BOOLEAN NOT NULL DEFAULT FALSE,
    `forCourse` BOOLEAN NOT NULL DEFAULT FALSE,
    `createdAt` DATETIME DEFAULT NOW(),
    `updatedAt` DATETIME DEFAULT NOW(),
    PRIMARY KEY (id)
);
