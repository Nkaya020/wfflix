CREATE TABLE entries (
       `id` INT UNIQUE NOT NULL AUTO_INCREMENT,
       `userId` INT NOT NULL,
       `courseId` INT NOT NULL,
       PRIMARY KEY(id)
);
