<?php

function db(array $pdoOptions = [
    PDO::MYSQL_ATTR_FOUND_ROWS => true,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]) {
    $ini = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . "/config.ini", true)[gethostname()];
    $db = new PDO('mysql:host=' . $ini['host'] . ';dbname=' . $ini['dbname'],
        $ini['user'], $ini['password'], $pdoOptions);
    return $db;
}

/**
 * @param string $query
 * Deze string is de databasequery. Let op dat deze nog geen
 * gebruikersinput bevat. Je kan gebruikersinput markeren
 * met ?'s. Deze ?'s worden op volgorde vervangen met de
 * waardes in $vars.
 *
 * @param array $vars
 * Dit is een array met waardes die in de query komen staan.
 *
 * @return int
 * Deze functie returnt de ID van het object wat in de
 * database is gezet.
 */
function insertOne(PDO $db, string $query, $vars)
{
    $statement = $db->prepare($query);
    $statement->execute($vars);
    return $db->lastInsertId();
}


/**
 * De query and vars variabelen doen hetzelfde als de
 * bovenstaande functie.
 *
 * @return array
 * Deze functie returnt een array van database-data wat
 * moet worden omgezet tot objecten.
 */

function findMany(PDO $db, string $query, $vars)
{
    $statement = $db->prepare($query);
    $statement->execute($vars);
    return $statement->fetchAll();
}

/**
 * Idem dito.
 *
 * @return object
 * Deze functie returnt een enkele map wat omgezet moet
 * worden tot een enkel object.
 * @throws Exception
 */
function findSingle(PDO $db, string $query, $vars)
{
    $statement = $db->prepare($query);
    $statement->execute($vars);
    $rc = $statement->rowCount();
    if ($rc > 1) {
        throw new Exception("Found multiple results. <br>Query: " . $query . "<br>Vars: " . print_r($vars, true));
    }
    if ($rc < 1) {
        throw new Exception("Found no results. <br>Query: " . $query . "<br>Vars: " . print_r($vars, true));
    }
    $result = $statement->fetch();
    return $result;
}

?>
