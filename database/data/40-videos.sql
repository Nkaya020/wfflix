INSERT INTO videos
(title, description, url, thumbnail, makerId, course)
VALUES
( 'Ruby Installeren',
'Een korte video over hoe je de Ruby programmeertaal kan installeren.',
'https://www.youtube.com/embed/Mz8l35q2wRE',
'https://upload.wikimedia.org/wikipedia/commons/7/73/Ruby_logo.svg',
1,
1 );
INSERT INTO videos
(title, description, url, thumbnail, makerId, course)
VALUES
( 'Ruby Meet&Greet',
  'Een korte video over wie Ruby programmeertaal is.',
  'https://www.youtube.com/embed/U3wZojMWvzU',
  'https://upload.wikimedia.org/wikipedia/commons/7/73/Ruby_logo.svg',
  1,
  1 );
INSERT INTO videos
(title, description, url, thumbnail, makerId, course)
VALUES
( 'Ruby Why you should',
  'Een korte video waarom Ruby leren een must is',
  'https://www.youtube.com/embed/DRxvIYRljxM',
  'https://upload.wikimedia.org/wikipedia/commons/7/73/Ruby_logo.svg',
  1,
  1 );
INSERT INTO videos
(title, description, url, thumbnail, makerId, course)
VALUES
( 'Tedx',
  '“1400 why should I learn how to read “
“2028 why should I learn how to program “
Denk is hier maar over na.',
  'https://www.youtube.com/embed/xfBWk4nw440',
  'https://upload.wikimedia.org/wikipedia/commons/8/8f/Listing1.jpg',
  1,
  2 );
INSERT INTO videos
(title, description, url, thumbnail, makerId, course)
VALUES
( 'The art of code',
  'Software and technology has changed every aspect of the world we live in.',
  'https://www.youtube.com/embed/6avJHaC3C2U',
  'https://upload.wikimedia.org/wikipedia/commons/8/8f/Listing1.jpg',
  1,
  2 );
INSERT INTO videos
(title, description, url, thumbnail, makerId, course)
VALUES
( 'Alpha go',
  'Mens vs mens gemaakte A.I',
  'https://www.youtube.com/embed/WXuK6gekU1Y',
  'https://upload.wikimedia.org/wikipedia/commons/8/8f/Listing1.jpg',
  1,
  2 );