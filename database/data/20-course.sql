INSERT INTO courses
    (name, thumbnail, description, makerId, difficulty, category)
VALUES ('Ruby voor beginners',
        'https://upload.wikimedia.org/wikipedia/commons/7/73/Ruby_logo.svg',
        'hier leer je heel veel van',
        1,
        60,
        6);
INSERT INTO courses
(name, thumbnail, description, makerId, difficulty, category)
VALUES ('Algemene kennis coding',
        'https://upload.wikimedia.org/wikipedia/commons/8/8f/Listing1.jpg',
        'Wat is programmeren?',
        1,
        60,
        6);