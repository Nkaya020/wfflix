SET foreign_key_checks = 0;
DROP TABLE videos, categories, reviews, users, courses, entries;
SET foreign_key_checks = 1;