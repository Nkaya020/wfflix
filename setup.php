<?php
require_once 'database/require.php';
require_once 'models/00-model.php';
require_once 'models/User.php';

$db = db();
$db->beginTransaction();

function run_script($script, $db) {
    if (!file_exists($script)) {
        echo "!!! File " . $script . " does not exist. !!!";
        die();
    }
    $text = file_get_contents($script);
    if ($text == null || $text == "") {
        echo "Warning: " . $script . " is empty.<br>";
    }
    $result = $db->exec($text);
    if ($result === false) {
        echo " !!! SQL Script " . $script . " failed to run. !!!<br>";
        die(var_dump($db->errorInfo()));
    } else {
        echo "SQL Script " . $script . " ran succesfully. <br>";
    }
}

if (isset($_GET['script'])) {
    echo "Running exclusively " . $_GET['script'] . "<br>";
    run_script("database/" . $_GET['script'], $db);
    die();
}

run_script("database/clear.sql", $db);

foreach(glob("database/structure/*.sql") as $script) {
    run_script($script, $db);
}

foreach(glob("database/relations/*.sql") as $script) {
    run_script($script, $db);
}

foreach(glob("database/data/*.sql") as $script) {
    run_script($script, $db);
}


$stmt = $db->prepare('SELECT * FROM users');
$stmt->execute();
foreach($stmt->fetchAll() as $userObj) {
    $user = User::fill($userObj);
    $user->password = password_hash($user->password, PASSWORD_DEFAULT);
    $user->save($db);
}
echo "Hashed passwords.";

$db->commit();