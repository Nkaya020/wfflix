<?php
class Review extends Model {

    public int $id, $userId, $courseId, $videoId;
    public string $text, $title;
    public $forVideo, $forCourse;
    public $createdAt, $updatedAt;

    public static function getTableName()
    {
        return 'reviews';
    }

    public static function getPrimaryKeyName()
    {
        return 'id';
    }
}