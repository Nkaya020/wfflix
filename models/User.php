<?php
class User extends Model {

    public int $id;
    public string $username;
    public string $password;
    public string $question;
    public string $questionAnswer;
    public int $isMod;
    public int $isMaker;
    public int $isAdmin;

    /**
     * @param $name
     * @return self
     */
    public static function fromName($name) {
        require_once 'database/require.php';
        $db = db();
        $stmt = $db->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute([$name]);
        $obj =  User::fill($stmt->fetch());
        return $obj;
    }

    public static function getTableName()
    {
        return "users";
    }

    public static function getPrimaryKeyName()
    {
        return "id";
    }
}