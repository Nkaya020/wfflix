<?php /** @noinspection PhpUndefinedMethodInspection */

abstract class Model
{
    /**
     * @param $result
     * String map. This should work with POST, GET and database fetches.
     * @return self
     * Object of called class with filled values where possible.
     */
    public static function fill($result)
    {
        $class = static::class;
        $object = new $class;
        foreach ($result as $key => $value) {
            // check if the value is weird and adjust it
            // checkboxes from GET forms
            if ($value == "on") $value = true;
            $object->{$key} = $value;
        }
        return $object;
    }

    /**
     * @param $result
     * @return $this
     */
    public function fillMe($result) {
        $class = static::class;
        $class::fill($this, $result);
        return $this;
    }

    /**
     * @param PDO $db
     * When in doubt, use db();
     * @param $requirements
     * Requirements for selection (where clause.)
     * Example: "name LIKE ? AND isMod = true"
     * @param array $vars
     * Query parameters. Use this to wrap user input.
     * Example: ["kaas"] results in "name LIKE 'kaas' AND isMod = true"
     * @param string $columns
     * Columns to select.
     * Example: ["id", "username", "password"]
     * @return array
     */
    public static function find(PDO $db, $requirements, $vars = [], $columns = "*")
    {
        $class = static::class;

        // Array processing; overkill!
//        $req = "";
//        for($i = 0; $i < sizeof($requirements) - 1; $i++) {
//            $req = $req . $requirements[$i] . " AND ";
//        }
//        $req = $req . $requirements[sizeof($requirements) - 1];


        $stmt = $db->prepare("SELECT {$columns} FROM {$class::getTableName()} WHERE {$requirements}");
        $stmt->execute($vars);
        $result = array();
        foreach ($stmt->fetchAll() as $item) {
            array_push($result, $class::fill($item));
        }
        return $result;
    }

    /**
     * @param PDO $db
     * When in doubt, use db();
     * @param string $columns
     * Columns to select.
     * Example: ["id", "username", "password"]
     * @return array with items
     */
    public static function findAll(PDO $db, $columns = "*")
    {
        $class = static::class;
        $stmt = $db->prepare("SELECT {$columns} FROM {$class::getTableName()}");
        $stmt->execute(null);
        $result = array();
        foreach ($stmt->fetchAll() as $item) {
            array_push($result, $class::fill($item));
        }
        return $result;
    }

    /**
     * Find a single object from the database based on ID and return it.
     * @param PDO $db
     * When in doubt, use db();
     * @param $idValue
     * Value for the primary key. Compounds not supported yet!
     * @return void with single item
     * @throws Exception when there is no result or many results.
     */
    public static function findById(PDO $db, $idValue, $columns="*")
    {
        $class = static::class;
        $stmt = $db->prepare("SELECT {$columns} FROM {$class::getTableName()} WHERE {$class::getPrimaryKeyName()} = ?");
        $stmt->execute([$idValue]);
        if ($stmt->rowCount() != 1) throw new Exception("Query returned wrong amount of rows. 
         Expected: 1, Received: " . $stmt->rowCount());
        return $class::fill([$stmt->fetch()]);
    }

    /**
     * Save this object into the database bound by it's ID.
     * @param PDO $db
     * @return $this
     * @throws Exception on database errors.
     */
    public function save(PDO $db)
    {
        $values = array();
        $query = "UPDATE {$this->getTableName()} SET ";
        foreach ($this as $key => $value) {
            if (property_exists(static::class, $key) && isset($this->{$key})) {
                $query = $query . $key . " = ?, ";
                array_push($values, $value);
            }
        }
        array_push($values, $this->{$this->getPrimaryKeyName()});

        $query = substr($query, 0, strlen($query) - 2);
        $query = $query . " WHERE id = ?";
        $stmt = $db->prepare($query);

        $stmt->execute($values);

        if ($stmt->rowCount() != 1) throw new Exception("Query returned wrong amount of rows. Query: \"" . $query . "\" Vars: "
            . print_r($values, true) . "Expected: 1, Received: " . $stmt->rowCount());
        return $this;
    }

    public function saveNew(PDO $db) {
        $class = static::class;
        $vars = array();
       $prequery = "INSERT INTO {$this->getTableName()} ("; 
       $postquery = ") VALUES (";
       foreach($this as $key => $value) {
           if (property_exists(static::class, $key) && isset($this->{$key})) {
               $prequery = $prequery . $key . ", ";
               $postquery = $postquery . "?, ";
               array_push($vars, $value);
           }
       }
       $prequery = substr($prequery, 0, strlen($prequery) - 2);
       $postquery = substr($postquery, 0, strlen($postquery) - 2) . ");";
       $query = $prequery . $postquery;
       $stmt = $db->prepare($query);
       $stmt->execute($vars);
       
        if ($stmt->rowCount() != 1) throw new Exception("Query returned wrong amount of rows. Query: \"" . $query . "\" Vars: "
            . print_r($vars, true) . "Expected: 1, Received: " . $stmt->rowCount());

        $this->{$class::getPrimaryKeyName()} = $db->lastInsertId();
    }

    public abstract static function getTableName();

    public abstract static function getPrimaryKeyName();

}

?>
