<?php

class Course extends Model {

    public int $id;
    public string $thumbnail;
    public string $name;
    public string $description;
    public int $user;
    public int $difficulty;
    public int $category;
    public int $makerId;

    public static function getTableName()
    {
        return "courses";
    }

    public static function getPrimaryKeyName()
    {
        return "id";
    }

    public static function getForeignKeyHandle()
    {
        return "courseId";
    }
}