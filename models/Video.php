<?php
class Video extends Model {

    public int $id, $makerId, $course;
    public string $title, $description, $url, $thumbnail;

    public static function getTableName()
    {
        return 'videos';
    }

    public static function getPrimaryKeyName()
    {
        return 'id';
    }

    public function getReviews(){
        $reviews = Review::find(db(), "videoId = ?", [$this->id]);
        return $reviews;
    }
}