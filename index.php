<?php
session_start();

foreach(glob("models/*.php") as $script) {
    include $script;
}

foreach(glob("controllers/*.php") as $script) {
    include $script;
}

include 'core/Router.php';
$TITLE="WFFlix";

$router = new Router();
$uriCurrent = $_SERVER['REQUEST_URI'];
$methodCurrent = $_SERVER['REQUEST_METHOD'];
$router->direct($uriCurrent, $methodCurrent);
