<?php
class CourseController
{
    public static function newCourse() {
        $TITLE="WFFlix: Alle videos";
        require 'views/courses/new.view.php';
    }

    public static function addCourse() {
        require("database/require.php");
        $course = Course::fill($_POST);
        $course->makerId = $_SESSION['uid'];
        $course->difficulty = 5;
        $course->category = 1;
        $course->saveNew(db());
        header("location: /courses?success");
    }

    public static function allCourses() {
        require("database/require.php");
        $TITLE="WFFlix: Alle cursussen";
        $courses = Course::findAll(db());
        require 'views/courses/all.view.php';
    }

    public static function myCourses($uid) {
        require("database/require.php");
        $TITLE="WFFlix: Alle cursussen";
        $courses = Course::find(db(), "makerId = ?", [$uid]);
        require 'views/courses/all.view.php';
    }

    public static function showCourse(int $courseID){
        require("database/require.php");
        $db = db();
        $stmt = $db->prepare('SELECT * FROM courses WHERE id = ?');
        $stmt->execute([$courseID]);
        $course = Course::fill($stmt->fetch());
        $stmt = $db->prepare('SELECT * FROM users WHERE id = ?');
        $stmt->execute([$course->makerId]);
        $owner = User::fill($stmt->fetch());
        $videos = Video::find(db(), "course = ?", [$courseID]);
        $TITLE="WFFlix";
        include 'views/videos/videos.view.php';
    }
}
?>