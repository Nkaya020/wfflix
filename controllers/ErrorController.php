<?php
class ErrorController {
    public static function e404() {
        include 'views/errors/404.php';
    }

    public static function e403() {
        include 'views/errors/403.php';
    }
}
?>
