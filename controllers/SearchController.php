<?php
class SearchController {
    public static function search($query) {
        require 'database/require.php';
        global $TITLE, $videos, $courses;
        $TITLE = "WFFlix: zoek " . $_GET['query'];
        $query = '%' . str_replace(' ','%', $query) . '%';
        $db = db();
        $stmt = $db->prepare('SELECT * FROM videos WHERE title LIKE :query OR description LIKE :query');
        $stmt->execute(['query' => $query]);
        $videos = $stmt->fetchAll();
        $stmt = $db->prepare('SELECT * FROM courses WHERE name LIKE :query');
        $stmt->execute(['query' => $query]);
        $courses = $stmt->fetchAll();
        include 'views/search.view.php';
    }
}