<?php

class AccountController
{
    public static function newUser() {
        require ('database/require.php');
        if ($_POST['password'] !== $_POST['password2']) {
            header("/account/new?fail");
            die();
        }
        $user = User::fill($_POST);
        $user->password = password_hash($user->password, PASSWORD_DEFAULT);
        $user->saveNew(db());
        header("location:/account/login");
    }

    public static function registration() {
        global $TITLE;
        $TITLE="WFFlix: nieuw account";
        include 'views/registration.view.php';
    }
    
    public static function landingPage()
    {
        $TITLE="WFFlix: Account van " . $_SESSION['username'];
        include("views/landing.view.php");
    }

    public static function logout()
    {
        session_destroy();
        header('location:/home');
    }

    public static function editMyAccount()
    {
        require("database/require.php");
        $TITLE="WFFlix: account aanpassen";
        global $destination, $users;
        $destination = "/account/update";
        $users = [User::fromName($_SESSION['username'])];
        include "views/management/accounts.view.php";
    }

    public static function login()
    {
        $TITLE = "WFFlix: log in";
        require_once 'views/login.view.php';
    }

    public static function postLogin()
    {
        require_once ("database/require.php");
        $pass = $_POST['password'];
        $user = User::fromName($_POST['username']);
        if (!isset($user->id)) {
            header('location:/account/login?fail');
        }
        if (password_verify($pass, $user->password)) {
            $_SESSION['loggedIn'] = true;
            if ($user->isMaker) $_SESSION['isMaker'] = $user->isMaker;
            if ($user->isMod) $_SESSION['isMod'] = $user->isMod;
            if ($user->isAdmin) $_SESSION['isAdmin'] = $user->isAdmin;
            $_SESSION['username'] = $user->username;
            $_SESSION['uid'] = $user->id;

            header('location:/home');
        } else {
            header('location:/account/login?fail');
        }
    }

    public static function accountUpdate() {
        require("database/require.php");
        try {
            $user = User::fill($_POST);
            if ($user->password !== "") {
                $user->password = password_hash($user->password, PASSWORD_DEFAULT);
            } else {
                unset($user->password);
            }
            $user->save(db());
        } catch (Exception $e) {
            header("location:/admin/accounts?fail");
            exit();
        }
        header("location:/admin/accounts");
        exit();
    }

    public static function accountOverview() {
        global $destination, $users, $TITLE;
        require('database/require.php');
        $TITLE="WFFlix: Gebruikers";
        $destination = "/admin/accounts/update";
        $users = User::findAll(db());
        require('views/management/accounts.view.php');
    }

}
