<?php


class HomeController
{
    public static function index()
    {
        $TITLE="WFFlix: lerend kijken";
        //require 'assets/prefix.php';
        //require 'assets/header.php';
        require("database/require.php");

        $db = db();
        $stmt = $db->prepare("SELECT * FROM videos ORDER BY id DESC LIMIT 3");
        $stmt->execute();
        $videos = [];
        foreach ($stmt->fetchAll() as $video) {
            array_push($videos, Video::fill($video));
        }

        require 'views/home.view.php';
        //require 'assets/suffix.php';
    }
}