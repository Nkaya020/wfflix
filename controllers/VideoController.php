<?php

class VideoController {

    public static function newVideo() {
        require("database/require.php");
        $TITLE="WFFlix: Alle videos";
        $courses = Course::find(db(), 'makerId = ?', [$_SESSION['uid']]);
        require 'views/videos/new.view.php';
    }

    public static function addVideo() {
        require("database/require.php");
        $video = Video::fill($_POST);
        $video->makerId = $_SESSION['uid'];
        $video->saveNew(db());
        header("location: /videos?success");
    }

    public static function allVideos() {
        require("database/require.php");
        $TITLE="WFFlix: Alle videos";
        $videos = Video::findAll(db());
        require 'views/videos/videos.view.php';
    }

    public static function myVideos($uid) {
        require("database/require.php");
        $TITLE="WFFlix: Alle videos";
        $videos = Video::find(db(), "makerId = ?", [$uid]);
        require 'views/videos/videos.view.php';
    }

    public static function showVideo(int $videoID){
        require("database/require.php");

        $db = db([PDO::ATTR_FETCH_TABLE_NAMES=>true, PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC]);
        $stmt = $db->prepare("SELECT *, o.username maker, r.id IS NOT NULL hasReviews FROM videos v
                                LEFT OUTER JOIN reviews r on v.id = r.videoId
                                LEFT OUTER JOIN users u on r.userId = u.id
                                LEFT OUTER JOIN users o on o.id = v.makerId
                                WHERE v.id=?");
        $stmt->execute([$videoID]);
        $reviews = $stmt->fetchAll();

        $TITLE="WFFlix: " . $reviews[0]["v.title"];
        include 'views/videos/show.view.php';
    }
}


?>